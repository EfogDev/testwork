"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var EventLayoutService = function () {
    function EventLayoutService() {
        _classCallCheck(this, EventLayoutService);

        this._eventsList = [];
    }

    /**
     * Add events to service
     */


    _createClass(EventLayoutService, [{
        key: "addEvents",
        value: function addEvents(events) {
            if (!Array.isArray(events)) {
                events = [events];
            }

            this._eventsList = this._eventsList.concat(events);

            return this;
        }

        /**
         * Arrange group of intersacting events to cols
         *
         * @return Array of cols with events
         */

    }, {
        key: "arrangeIntersectingEventsToCols",
        value: function arrangeIntersectingEventsToCols(events) {
            this.sortEvents(events);

            var cols = [[]];

            events.map(function (event) {
                var isAddedToExistingCol = false;

                cols.map(function (col) {
                    if (isAddedToExistingCol) {
                        return;
                    }

                    var colLastEvent = col.slice(-1)[0];

                    if (!colLastEvent || !areEventsIntersecting(event, colLastEvent)) {
                        col.push(event);
                        isAddedToExistingCol = true;
                    }
                });

                if (!isAddedToExistingCol) {
                    cols.push([event]);
                }
            });

            return cols;
        }

        /**
         * Get events ordered in intersection groups and columns
         */

    }, {
        key: "getEventsLayout",
        value: function getEventsLayout() {
            var _this = this;

            return this.getIntersections().map(function (intersectionGroup) {
                return _this.arrangeIntersectingEventsToCols(intersectionGroup);
            });
        }

        /**
         * Order added events by start and duration
         */

    }, {
        key: "sortEvents",
        value: function sortEvents(list) {
            list = list || this._eventsList;

            list.sort(function (a, b) {
                return a.start - b.start || a.duration - b.duration;
            });

            return this;
        }

        /**
         * Groups added events to intersecting groups
         *
         * @return Array of intersecting events groups
         */

    }, {
        key: "getIntersections",
        value: function getIntersections() {
            this.sortEvents();

            return this._eventsList.reduce(function (list, event) {
                if (!list.length) {
                    list.push([event]);
                } else {
                    var addedToExistingList = false;

                    list.map(function (intersectinEvents) {
                        return intersectinEvents.map(function (eventFromList) {
                            if (areEventsIntersecting(event, eventFromList)) {
                                intersectinEvents.push(event);
                                addedToExistingList = true;
                            }
                        });
                    });

                    if (!addedToExistingList) {
                        list.push([event]);
                    }
                }

                return list;
            }, []);
        }
    }]);

    return EventLayoutService;
}();

/**
 * Check event intersection
 */


function areEventsIntersecting(event1, event2) {
    var events = [event1, event2].sort(function (a, b) {
        return a.start - b.start;
    });

    return events[0].start + events[0].duration > events[1].start;
}
