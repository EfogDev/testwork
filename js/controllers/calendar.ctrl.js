angular.module('testwork')

    .controller('CalendarCtrl', function ($scope) {
        $scope.eventList = $scope.events();
        $scope.allEvents = [];

        $scope.day = {
            start: new Date(0, 0, 0, 8, 0, 0, 0),
            end: new Date(0, 0, 0, 17, 0, 0, 0)
        };

        $scope.generatePeriods = function () {
            var periods = [];

            var hours = $scope.day.end.getHours() - $scope.day.start.getHours();
            var startHour = $scope.day.start.getHours();

            var periodsCount = hours * 2;
            for (var i = 0; i <= periodsCount; i++) {
                var currentHour = parseInt(i / 2);

                periods.push({label: (startHour + currentHour).toString() + (i % 2 == 0 ? ':00' : ':30')});
            }

            $scope.periods = periods;
        };

        $scope.renderEvents = function () {
            $scope.allEvents.length = 0;

            $scope.eventList.map(function (intersectionGroup) {
                return intersectionGroup.map(function (column, columnIndex) {
                    return column.map(function (event) {
                        var getEventWidth = function (event) {
                            return 100 / (intersectionGroup.length);
                        };

                        var getEventLeft = function (event) {
                            return getEventWidth(event) * columnIndex;
                        };

                        var getEventHeight = function (event) {
                            return event.duration * 100 / 30;
                        };

                        var getEventTop = function (event) {
                            return event.start * 100 / 30;
                        };

                        event.style = {
                            width: getEventWidth(event) + '%',
                            height: getEventHeight(event) + '%',
                            left: getEventLeft(event) + '%',
                            top: getEventTop(event) + '%'
                        };

                        $scope.allEvents.push(event);
                    });
                });
            });
        };

        $scope.generatePeriods();

        $scope.$watch(function () {
            return $scope.events();
        }, function (events) {
            $scope.eventList = events;
            $scope.renderEvents();
        });
    })

;
