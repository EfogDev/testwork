angular.module('testwork')

    .service('Events', function () {
        var index = 0;

        var events = [
            {id: index++, start: 0,  duration: 15, title: "Exercise"},
            {id: index++, start: 25, duration: 30, title: "Travel to work"},
            {id: index++, start: 30, duration: 30, title: "Plan day"},
            {id: index++, start: 60, duration: 15, title: "Review yesterday's commits"},
            {id: index++, start: 100,  duration: 15, title: "Code review"},
            {id: index++, start: 180,  duration: 90, title: "Have lunch with John"},
            {id: index++, start: 360,  duration: 30, title: "Skype call"},
            {id: index++, start: 370,  duration: 45, title: "Follow up with designer"},
            {id: index++, start: 405,  duration: 30, title: "Push up branch"},
        ];

        var counted = new EventLayoutService().addEvents(events).getEventsLayout();

        this.get = function () {
            return counted;
        };
    })

;
